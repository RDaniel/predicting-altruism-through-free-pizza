import spacy

_nlp = spacy.load('en_core_web_sm')


class PreprocessingStep:

    def name(self):
        pass

    def process(self, corpus):
        pass


class PreprocessingPipeline:

    def __init__(self, preprocess_steps):
        self.preprocess_steps = preprocess_steps

    def run(self, corpora):

        for step in self.preprocess_steps:
            for corpus in corpora:
                step.process(corpus)


class LemmatizationStep(PreprocessingStep):

    def name(self):
        return "Lemmatization"

    def process(self, corpus):
        for request in corpus:
            doc = _nlp(request.text)
            request.text = ' '.join([token.lemma_ for token in doc])


class StopWordsRemovalStep(PreprocessingStep):

    def name(self):
        return "Stop words removal"

    def process(self, corpus):
        for request in corpus:
            doc = _nlp(request.text)
            request.text = ' '.join([token.text for token in doc if not token.is_stop])


class LowerCaseStep(PreprocessingStep):

    def name(self):
        return "Lower case converter"

    def process(self, corpus):
        for request in corpus:
            request.text = request.text.lower()
