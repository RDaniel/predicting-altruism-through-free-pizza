import abc
import os

import numpy as np
import spacy
from gensim.models import KeyedVectors
from keras.layers import Embedding
from keras_preprocessing.sequence import pad_sequences
from keras_preprocessing.text import Tokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter

from data.data import Dataset, Corpus

'''Objects for transforming corpuses
   into a numerical representation.'''

nlp = spacy.load("en_core_web_sm")


def get_embedding(encoder):
    try:
        return encoder.embedding_layer
    except AttributeError:
        raise ValueError('This encoder does not possess an embedding.')


class CorpusEncoder(abc.ABC):

    @abc.abstractmethod
    def fit(self, corpus, **kwargs):
        pass

    def transform(self, corpus, **kwargs):
        pass

    def fit_transform(self, corpus, **kwargs):
        self.fit(corpus, **kwargs)
        return self.transform(corpus, **kwargs)


class TFIDFEncoder(CorpusEncoder):

    def __init__(self, max_df=1.0, min_df=0.0):
        self.vectorizer = TfidfVectorizer(max_df=max_df, min_df=min_df)

    def fit(self, corpus, **kwargs):
        texts = [request.text for request in corpus]
        self.vectorizer.fit(texts)

    def transform(self, corpus, **kwargs):
        X = self.vectorizer.transform([request.text for request in corpus])
        return Dataset(X.todense(), corpus.labels)


class NumericAttributeEncoder(CorpusEncoder):

    def __init__(self, features_to_ignore=None):
        self.features_to_ignore = [] if features_to_ignore is None else features_to_ignore

    def fit(self, corpus, **kwargs):
        pass

    def transform(self, corpus, **kwargs):
        numeric_X = []
        numeric_y = []

        for request in corpus:
            features = vars(request).copy()
            y = features.pop('received_pizza')
            features.pop('text')
            for feature_to_ignore in self.features_to_ignore:
                features.pop(feature_to_ignore)
            numeric_X.append(list(features.values()))
            numeric_y.append(y)

        return Dataset(numeric_X, numeric_y)


class EncodingConcatenator(CorpusEncoder):

    def __init__(self, corpus_encoders):
        self.corpus_encoders = corpus_encoders
    
    def fit(self, corpus, **kwargs):
        for corpus_encoder in self.corpus_encoders:
            corpus_encoder.fit(corpus)
        
    def transform(self, corpus, **kwargs):
        datasets = [corpus_encoder.transform(corpus) for corpus_encoder in self.corpus_encoders]
        return Dataset(np.concatenate([dataset.X for dataset in datasets], axis=1), datasets[0].y)


class IndexEncoder(CorpusEncoder):

    def __init__(self, num_words=10000, max_seq_len=100, input_dim=300):
        self.tokenizer = Tokenizer(num_words=num_words)
        self.max_seq_len = max_seq_len
        self.input_dim = input_dim

    def fit(self, corpus, **kwargs):
        self.tokenizer.fit_on_texts([request.text for request in corpus])
        vocab_size = len(self.tokenizer.word_index) + 1
        self.embedding_layer = Embedding(vocab_size, self.input_dim, input_length=self.max_seq_len, trainable=True)

    def transform(self, corpus, **kwargs):
        sequences = self.tokenizer.texts_to_sequences([request.text for request in corpus])
        sequences = pad_sequences(sequences, padding='post', maxlen=self.max_seq_len)
        return Dataset(sequences, corpus.labels)


class GloveEncoder(CorpusEncoder):

    def __init__(self, glove_filepath, num_words=10000, input_dim=300, max_seq_len=100):
        self.embeddings_dictionary = dict()
        self.tokenizer = Tokenizer(num_words=num_words)
        self.glove_filepath = glove_filepath
        self.input_dim = input_dim
        self.max_seq_len = max_seq_len
    
    def fit(self, corpus, **kwargs):
        self.tokenizer.fit_on_texts([request.text for request in corpus])
        vocab_size = len(self.tokenizer.word_index) + 1

        with open(self.glove_filepath, encoding="utf8") as glove_file:
            for line in glove_file:
                records = line.split()
                word = records[0]
                vector_dimensions = np.asarray(records[1:], dtype='float32')
                self.embeddings_dictionary[word] = vector_dimensions
        
        self.embedding_matrix = np.zeros((vocab_size, self.input_dim))
        for word, index in self.tokenizer.word_index.items():
            embedding_vector = self.embeddings_dictionary.get(word)
            if embedding_vector is not None:
                self.embedding_matrix[index] = embedding_vector
            
        self.embedding_layer = Embedding(vocab_size, self.input_dim, weights=[self.embedding_matrix],
                                         input_length=self.max_seq_len, trainable=False)
    
    def transform(self, corpus, **kwargs):
        semantic_composition = kwargs.get('semantic', False)

        if not semantic_composition:
            sequences = self.tokenizer.texts_to_sequences([request.text for request in corpus])
            sequences = pad_sequences(sequences, padding='post', maxlen=self.max_seq_len)
            return Dataset(sequences, corpus.labels)
        
        if semantic_composition:
            res_array = np.zeros((len(corpus), self.input_dim))
            for i, request in enumerate(corpus):
                words = request.text.split()
                for word in words[:self.max_seq_len]:
                    try:
                        res_array[i] += self.embeddings_dictionary[word]
                    except KeyError:
                        pass
                res_array[i] /= min(len(words), self.max_seq_len) + 1
            return Dataset(res_array, corpus.labels)
            

class Word2VecEncoder(CorpusEncoder):

    def __init__(self, w2v_filepath, row_maxlen=200, trainable=False, limit=50_000):
        self.row_maxlen = row_maxlen
        self.model = KeyedVectors.load_word2vec_format(w2v_filepath, binary=True, limit=limit)
        self.trainable = trainable
        self._VECTOR_SIZE = 300

    def fit(self, corpus, **kwargs):
        self.embedding_layer = self.model.get_keras_embedding(self.trainable)

    def transform(self, corpus, **kwargs):
        semantic_composition = kwargs.get('semantic', False)

        if not semantic_composition:
            res_array = np.zeros((len(corpus), self.row_maxlen), dtype='int32')
            for i, request in enumerate(corpus):
                words = request.text.split()
                for j, word in enumerate(words[:self.row_maxlen]):
                    try:
                        res_array[i, j] = self.model.wv.vocab.get(word).index
                    except AttributeError:
                        res_array[i, j] = 0

        if semantic_composition:
            res_array = np.zeros((len(corpus), self._VECTOR_SIZE))
            for i, request in enumerate(corpus):
                words = request.text.split()
                for word in words[:self.row_maxlen]:
                    try:
                        res_array[i] += self.model.wv[word]
                    except KeyError:
                        pass
                res_array[i] /= min(len(words), self.row_maxlen) + 1

        return Dataset(res_array, corpus.labels)


if __name__ == '__main__':
    ind = TFIDFEncoder()
    corpus = Corpus(['I love rocks and trains and planes',
                     'I am dwayne the RoCk johnson'], np.array([1, 0]))
    res_array = ind.fit_transform(corpus)
    print(ind.vectorizer.get_feature_names())
    print(res_array)
