import pickle
import random

import numpy as np

from data.read_dataset import read_data


class Request:
    def __init__(self):
        self.text = None
        self.account_age = None
        self.days_on_raop = None
        self.comments = None
        self.comments_in_raop = None
        self.comments_in_raop_retrieval = None
        self.posts = None
        self.posts_in_raop = None
        self.posts_in_raop_retrieval = None
        self.subreddits = None
        self.upvotes_minus_downvotes = None
        self.upvotes_plus_downvotes = None
        self.upvotes_minus_downvotes_retrieval = None
        self.upvotes_plus_downvotes_retrieval = None
        self.timestamp = None
        self.received_pizza = None


class Corpus:
    """Represents only a collection of requests with appropriate labels,
       if they exist. Labels are already encoded."""

    def __init__(self, requests, labels):
        self.requests = requests
        self.labels = labels

    def __iter__(self):
        return iter(self.requests)

    def __len__(self):
        return len(self.requests)

    def __getitem__(self, index):
        return self.requests[index]

    def __setitem__(self, index, value):
        self.requests[index] = value

    def save(self, filename):
        with open(filename, 'wb') as outfile:
            pickle.dump(self, outfile)

    @staticmethod
    def load(filename):
        with open(filename, 'rb') as readfile:
            return pickle.load(readfile)


class Dataset:
    """Contains only numeric data as numpy arrays."""

    def __init__(self, X, y):
        self.X = np.array(X)
        self.y = np.array(y)

    def __len__(self):
        return len(self.y)

    def __str__(self):
        return str(self.X)

    def save(self, filename):
        with open(filename, 'wb') as outfile:
            pickle.dump(self, outfile)

    @staticmethod
    def load(filename):
        with open(filename, 'rb') as readfile:
            return pickle.load(readfile)


def create_request_corpus(data_file):
    data = read_data(data_file)
    return extract_corpus(data)


def extract_corpus(json_dataset):
    train, test = [], []

    for entry in json_dataset:
        request = Request()
        request.text = entry['request_text']
        request.account_age = entry['requester_account_age_in_days_at_request']
        request.days_on_raop = entry['requester_days_since_first_post_on_raop_at_request']
        request.comments = entry['requester_number_of_comments_at_request']
        request.comments_in_raop = entry['requester_number_of_comments_in_raop_at_request']
        request.comments_in_raop_retrieval = entry['requester_number_of_comments_in_raop_at_retrieval']
        request.posts = entry['requester_number_of_posts_at_request']
        request.posts_in_raop = entry['requester_number_of_posts_on_raop_at_request']
        request.posts_in_raop_retrieval = entry['requester_number_of_posts_on_raop_at_retrieval']
        request.subreddits = entry['requester_number_of_subreddits_at_request']
        request.upvotes_minus_downvotes = entry['requester_upvotes_minus_downvotes_at_request']
        request.upvotes_plus_downvotes = entry['requester_upvotes_plus_downvotes_at_request']
        request.upvotes_minus_downvotes_retrieval = entry['requester_upvotes_minus_downvotes_at_retrieval']
        request.upvotes_plus_downvotes_retrieval = entry['requester_upvotes_plus_downvotes_at_retrieval']
        request.timestamp = entry['unix_timestamp_of_request_utc']
        
        request.received_pizza = 1 if entry['requester_received_pizza'] else 0

        if entry['in_test_set']:
            test.append(request)
        else:
            train.append(request)

    random.shuffle(train)
    random.shuffle(test)

    train_labels = [request.received_pizza for request in train]
    test_labels = [request.received_pizza for request in test]

    return Corpus(train, train_labels), Corpus(test, test_labels)
