import abc
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler
from yellowbrick.cluster import KElbowVisualizer

from keras.utils.np_utils import to_categorical
from model.model import Model
from keras import models
from keras.layers import Dense, LSTM, Input, concatenate

import model.machinelearning as ml 
import numpy as np

_metrics = ['accuracy']


class ClusteringClassifier(Model):
    def __init__(self, cluster_range=(2, 12)):
        self.cluster_range = cluster_range

    def _train_kmeans(self, X):
        self.kmeans = []
        self.normalizers = []
        for i in range(X.shape[1]):
            col = X[:, i].reshape(-1, 1)
            kmeans = KMeans()
            normalizer = StandardScaler()
            col = normalizer.fit_transform(col)
            self.normalizers.append(normalizer)
            visualizer = KElbowVisualizer(kmeans, metric='calinski_harabasz', k=self.cluster_range)   
            visualizer.fit(col)
            elbow = visualizer.elbow_value_
            elbow = 2 if elbow is None else elbow

            elbow_kmeans = KMeans(n_clusters=elbow)
            elbow_kmeans.fit(col)
            self.kmeans.append(elbow_kmeans)
        
    def _transform_to_cluster_space(self, X):
        onehots = []
        for i, (kmeans, normalizer) in enumerate(zip(self.kmeans, self.normalizers)):
            col = normalizer.transform(X[:, i].reshape(-1, 1))
            y = kmeans.predict(col)
            onehots.append(to_categorical(y))
        return onehots

    def name(self):
        return 'cluster_classifier'

    def fit(self, X, y=None, **kwargs):
        if 'cluster_dimensions' not in kwargs:
            raise ValueError('Dimensions to cluster required.')
        dims = kwargs.pop('cluster_dimensions')
        self._train_kmeans(dims)
        dims_clustered = self._transform_to_cluster_space(dims)

        cluster_model_params = kwargs['cluster_model_params']
        self.bottom_model = self.create_cluster_model(self.kmeans, **cluster_model_params)

        bottom_fit_params = kwargs['bottom_fit_params']
        self.bottom_model.fit(X, dims_clustered, **bottom_fit_params)

        classifier_params = kwargs['classifier_params']
        self.top_model = self.create_classifier(self.kmeans, **classifier_params)

        top_fit_params = kwargs['top_fit_params']
        cluster_predictions = self.bottom_model.predict(X)
        cluster_predictions = np.concatenate(cluster_predictions, axis=1)
        self.top_model.fit(cluster_predictions, y, **top_fit_params)
        
    def predict(self, X, **kwargs):
        X_clustered = self.bottom_model.predict(X, **kwargs)
        X_clustered = np.concatenate(X_clustered, axis=1)
        return self.top_model.predict(X_clustered, **kwargs)

    @abc.abstractmethod
    def create_cluster_model(self, kmeans_list, **kwargs):
        pass

    @abc.abstractmethod
    def create_classifier(self, kmeans_list, **kwargs):
        pass


def _cluster_lstm(kmeans, embedding, kmeans_list):
    text_input = Input(shape=(embedding.input_length,), dtype='int32')
    x = embedding(text_input)
    x = LSTM(32)(x)
    x = Dense(64, activation='relu')(x)
    x = Dense(64, activation='relu')(x)
    outputs = []
    for kmeans in kmeans:
        out = Dense(kmeans.n_clusters, activation='softmax')(x)
        outputs.append(out)
    lstm = models.Model(inputs=text_input, outputs=outputs)
    lstm.compile(optimizer='rmsprop', loss='categorical_crossentropy')
    oldfit = lstm.fit

    def fit_func(X, y, **kwargs):
        epochs_lstm = kwargs.pop('epochs_lstm')
        oldfit(X, y, epochs=epochs_lstm, **kwargs)

    lstm.fit = fit_func 
    
    return lstm


class ClusteringNN(ClusteringClassifier):

    def __init__(self, embedding):
        super().__init__()
        self.embedding = embedding
    
    @staticmethod
    def name():
        return 'clustering_nn'

    def create_cluster_model(self, kmeans_list, **kwargs):
        return _cluster_lstm(self.kmeans, self.embedding, kmeans_list)

    def create_classifier(self, kmeans_list, **kwargs):
        inputs_len = 0
        for kmeans in self.kmeans:
            inputs_len += kmeans.n_clusters

        inputs = Input(shape=(inputs_len,))
        x = Dense(64, activation='relu')(inputs)
        x = Dense(64, activation='relu')(x)
        out = Dense(1, activation='sigmoid')(x)
        top_model = models.Model(inputs=inputs, outputs=out)
        top_model.compile(optimizer='rmsprop', loss='binary_crossentropy', metrics=_metrics)

        oldfit = top_model.fit
        def fit_func(X, y, **kwargs):
            epochs_dense = kwargs.pop('epochs_dense')
            class_weight = kwargs.pop('class_weight', {1: 1, 0: 1})
            oldfit(X, y, epochs=epochs_dense, class_weight=class_weight, **kwargs)

        top_model.fit = fit_func

        return top_model


def create_cluster_classifier(basecls):

    class Cls(ClusteringClassifier):
        def __init__(self, embedding):
            super().__init__()
            self.embedding = embedding
    
        @staticmethod
        def name():
            return f'clustering_{basecls.name()}'

        def create_cluster_model(self, kmeans_list, **kwargs):
            return _cluster_lstm(self.kmeans, self.embedding, kmeans_list)

        def create_classifier(self, kmeans_list, **kwargs):
            return basecls(scoring=kwargs['scoring'], class_weight=kwargs['class_weight'])

    return Cls
    