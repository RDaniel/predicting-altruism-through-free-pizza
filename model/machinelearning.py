from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from xgboost import XGBClassifier

from .model import Model
import numpy as np


class LogisticRegression(Model):

    def __init__(self, **kwargs):
        self.model = LogisticRegressionCV(penalty='l2',
                                          max_iter=10000, cv=2, tol=0.0001, solver='lbfgs',
                                          **kwargs)

    @staticmethod
    def name():
        return 'logistic_regression'

    def fit(self, X, y=None, **kwargs):
        self.model.fit(X, y, **kwargs)

    def predict(self, X, **kwargs):
        return self.model.predict_proba(X)[:, 1]


class XGBoost(Model):

    def __init__(self, **kwargs):
        self.class_weight = kwargs.pop('class_weight')
        gradient_boosting = XGBClassifier(max_depth=3, n_estimators=200)
        self.clf = GridSearchCV(gradient_boosting,
                                verbose=5,
                                cv=5,
                                param_grid=[{'max_depth': range(1, 7, 2), 'n_estimators': range(10, 100, 20)}],
                                n_jobs=-1,
                                **kwargs)

    @staticmethod
    def name():
        return 'xgboost'

    def fit(self, X, y=None, **kwargs):
        sample_weight = np.zeros_like(y)
        sample_weight[y==0] = self.class_weight[0]
        sample_weight[y==1] = self.class_weight[1]
        self.clf.fit(X, y, sample_weight=sample_weight, **kwargs)

    def predict(self, X, **kwargs):
        return self.clf.predict_proba(X)[:, 1]


class SupportVectorClassifier(Model):

    def __init__(self, **kwargs):
        svc = SVC(kernel='rbf', class_weight=kwargs.pop('class_weight'))
        self.model = GridSearchCV(estimator=svc,
                                  cv=5,
                                  verbose=3,
                                  param_grid=[{'C': [0.001, 0.01, 0.1]}],
                                  n_jobs=-1,
                                  **kwargs)

    @staticmethod
    def name():
        return "svm_linear"

    def fit(self, X, y=None, **kwargs):
        self.model.fit(X, y, **kwargs)

    def predict(self, X, **kwargs):
        return self.model.predict(X)
