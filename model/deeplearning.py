from keras import models
from keras import optimizers
from keras.callbacks import Callback
from keras.layers import (Dense, Conv1D, MaxPooling1D, LSTM, GlobalMaxPooling1D, Dropout, Activation,
                          GlobalAveragePooling1D, Bidirectional)
from sklearn.metrics import recall_score

from .model import Model

_metrics = ['accuracy']


class MetricCallback(Callback):

    def __init__(self, metric, validation_data, coef=1):
        self.metric = metric
        self.val_data = validation_data
        self.coef = coef

    def on_train_begin(self, logs=None):
        self.best_weights = None
        self.best = None

    def on_epoch_end(self, epoch, logs=None):
        val_X, val_y = self.val_data
        y_pred = self.model.predict(val_X)
        y_pred = y_pred > 0.5

        score = self.metric(val_y, y_pred) * self.coef
        if not self.best or score > self.best:
            self.best = score * self.coef
            self.best_weights = self.model.get_weights()

    def on_train_end(self, logs=None):
        self.model.set_weights(self.best_weights)


class DeepLearningModel(Model):

    def fit(self, X, y=None, **kwargs):
        callback = MetricCallback(recall_score, kwargs['validation_data'])
        self.model.fit(X, y, verbose=2, callbacks=[callback], **kwargs)

    def predict(self, X, **kwargs):
        return self.model.predict(X, **kwargs)


class TwoLayerCNN(DeepLearningModel):

    def __init__(self, embedding, filters=250, kernel_size=5, pool_size=3,
                 hidden_size=100, dr=0.8):
        self.model = models.Sequential()
        self.model.add(embedding)
        self.model.add(Conv1D(filters,
                              kernel_size,
                              padding='valid',
                              activation='relu',
                              strides=1))
        self.model.add(MaxPooling1D(pool_size=pool_size))
        self.model.add(Conv1D(filters,
                              kernel_size,
                              padding='valid',
                              activation='relu',
                              strides=1))
        self.model.add(GlobalMaxPooling1D())
        self.model.add(Dense(hidden_size))
        self.model.add(Dropout(dr))
        self.model.add(Activation('relu'))
        self.model.add(Dense(1, activation='sigmoid'))
        self.model.compile(loss='binary_crossentropy',
                           optimizer='adam',
                           metrics=_metrics)

    @staticmethod
    def name():
        return 'two_layer_cnn'


class CNN_LSTM(DeepLearningModel):

    def __init__(self, embedding, lstm_width=64, l1=0.01, l2=0.01, hl_width=250):
        self.model = models.Sequential()
        self.model.add(embedding)
        self.model.add(Conv1D(256, 3, activation='relu', padding='same'))
        self.model.add(MaxPooling1D())
        self.model.add(LSTM(lstm_width))
        self.model.add(Dropout(0.25))
        self.model.add(Dense(1, name='out_layer'))
        self.model.add(Activation('sigmoid'))
        optimizer = optimizers.RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        self.model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=_metrics)
        self.model.summary()

    @staticmethod
    def name():
        return 'cnn_and_lstm'


class SingleLayerCNN(DeepLearningModel):

    def __init__(self, embedding):
        self.model = models.Sequential()
        self.model.add(embedding)
        self.model.add(Conv1D(128, 5, activation='relu'))
        self.model.add(GlobalMaxPooling1D())
        self.model.add(Dense(64, activation='tanh'))
        self.model.add(Dropout(0.25))
        self.model.add(Dense(1, activation='sigmoid'))

        optimizer = optimizers.RMSprop(lr=0.001)
        self.model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=_metrics)
        self.model.summary()

    @staticmethod
    def name():
        return 'single_layer_cnn'


class BiLSTM(DeepLearningModel):

    def __init__(self, embedding):
        self.model = models.Sequential()
        self.model.add(embedding)
        self.model.add(Bidirectional(LSTM(128, return_sequences=True)))
        self.model.add(GlobalAveragePooling1D())
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dropout(0.35))
        self.model.add(Dense(1, activation="sigmoid"))

        self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics=_metrics)
        self.model.summary()

    @staticmethod
    def name():
        return 'bidirectional_lstm'
