import abc


class Model(abc.ABC):

    @abc.abstractmethod
    def fit(self, X, y=None, **kwargs):
        pass

    @abc.abstractmethod
    def predict(self, X, **kwargs):
        pass