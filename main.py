import importlib
import json
import os
import shutil
import sys
from data.data import create_request_corpus, Corpus
from data.text_preprocessing import (PreprocessingPipeline, LowerCaseStep)
from experiments.utils import KFold, calculate_experiment_results, calculate_results_average
from keras import backend as K

'''
This script needs 2 input parameters:  number of repetitions of the experiment
and the name of the experiment module(s) (fully qualified, e.g. experiments.ml_experiments).
Need to look into how cross validation interacts with grid search so experiments can be measured statistically.
(IMPLEMENT KFOLD CV)

Creates a folder where the results of the experiments are made.
'''

'''USAGE EXAMPLE: python .\main.py 1 experiments.dl_experiments'''

DATA_FILE_NAME = 'data/pizza_request_dataset.json'

train, test = create_request_corpus(DATA_FILE_NAME)
data = Corpus(train.requests + test.requests, train.labels + test.labels)
pipeline = PreprocessingPipeline([LowerCaseStep()])
pipeline.run([data])

experiment_count = int(sys.argv[1])

n_folds = experiment_count
shuffle = False

kfold = KFold(n_folds, shuffle)
folds = kfold.create_folds(data)

for experiment_module_name in sys.argv[2:]:
    result_directory = f'experiments/{experiment_module_name.split(".")[-1]}'

    experiment_module = importlib.import_module(experiment_module_name)
    experiments = experiment_module.experiments

    results = {}
    i = 0
    for experiment_name in experiments:
        experiment_results = []
        for train, test in folds:
            y_true, y_pred = experiments[experiment_name].run(train, test)
            y_pred_binary = y_pred > 0.5

            fold_results = calculate_experiment_results(y_true, y_pred_binary)
            experiment_results.append(fold_results)

            K.clear_session()

        calculate_results_average(experiment_results)
        results[experiment_name] = experiment_results

    # if os.path.exists(result_directory):
    #     shutil.rmtree(result_directory)
    # os.makedirs(result_directory)

    for experiment in results:
        with open(f'{result_directory}/{experiment}.json', 'w') as result_file:
            json.dump(results[experiment], result_file)
