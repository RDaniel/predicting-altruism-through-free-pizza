import data.encoders as enc
import model.deeplearning as dl
import experiments.utils as utils
import sklearn.model_selection as ms

models = [
    dl.SingleLayerCNN,
    dl.TwoLayerCNN,
    dl.BiLSTM,
    dl.CNN_LSTM
]

encoders = {
    'glove': lambda: enc.GloveEncoder('embeddings/glove.6B.300d.txt'),
    'word2vec': lambda: enc.Word2VecEncoder('embeddings/vectors.bin')
}

experiments = {}

class Experiment:
    def __init__(self, model, encoder_name):
        self.model = model
        self.encoder_name = encoder_name

    def run(self, train, test):
        encoder = encoders[self.encoder_name]()             
        train = encoder.fit_transform(train)
        test = encoder.transform(test)
    
        class_weight = utils.balanced_class_weights(train)

        model_instance = self.model(enc.get_embedding(encoder))
        validation_split = 0.2

        x_train, x_val, y_train, y_val = ms.train_test_split(train.X, train.y, test_size=validation_split)
        model_instance.fit(x_train, y_train, epochs=20, batch_size=32, validation_data=(x_val, y_val), class_weight=class_weight) 

        del encoder

        y_true = test.y
        y_pred = model_instance.predict(test.X)

        return y_true, y_pred

for encoder_name in encoders:
    for model in models:
        experiments[f'{model.name()}_{encoder_name}'] = Experiment(model, encoder_name)