import collections

from sklearn.model_selection import StratifiedKFold

from data.data import Corpus

from sklearn.metrics import accuracy_score, precision_score, recall_score, fbeta_score


def balanced_class_weights(train):
    n_samples = len(train.y)
    n_classes = 2
    occurences = collections.Counter(train.y)

    class_weights = {0: n_samples / (n_classes * occurences[0]), 1: n_samples / (n_classes * occurences[1])}

    return class_weights


def calculate_experiment_results(y_true, y_pred_binary):
    return {
        'accuracy': accuracy_score(y_true, y_pred_binary),
        'precision': precision_score(y_true, y_pred_binary),
        'recall': recall_score(y_true, y_pred_binary),
        'f-1': fbeta_score(y_true, y_pred_binary, beta=1),
        'f-0.5': fbeta_score(y_true, y_pred_binary, beta=0.5),
        'f-2': fbeta_score(y_true, y_pred_binary, beta=2)
    }


def calculate_results_average(experiment_results):
    measures = {}
    for measure in experiment_results[0]:
        measure_key = "avg_" + measure
        measures[measure_key] = []
        for result in experiment_results:
            measures[measure_key].append(result[measure])

    for measure in measures:
        measures[measure] = sum(measures[measure]) / len(measures[measure])

    experiment_results.append(measures)


class KFold:

    def __init__(self, n_folds, shuffle=False):
        self.kfold = StratifiedKFold(n_folds, shuffle)

    def create_folds(self, data):
        folds = []
        for indices_train, indices_test in self.kfold.split(data.requests, data.labels):
            train_x, train_y = self._extract_data(data, indices_train)
            test_x, test_y = self._extract_data(data, indices_test)
            folds.append((Corpus(train_x, train_y), Corpus(test_x, test_y)))

        return folds

    def _extract_data(self, data, indices):
        x, y = [], []
        for i in indices:
            x.append(data.requests[i])
            y.append(data.labels[i])

        return x, y
