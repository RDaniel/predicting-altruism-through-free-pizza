import model.machinelearning as ml
import data.encoders as enc
import experiments.utils as utils

encoders = {
    'word2vec': lambda: enc.Word2VecEncoder('embeddings/vectors.bin'),
    'glove': lambda: enc.GloveEncoder('embeddings/glove.6B.300d.txt')
}

models = [
    ml.LogisticRegression,
    ml.SupportVectorClassifier,
    ml.XGBoost
]


experiments = {}

class Experiment:
    def __init__(self, model, encoder_name):
        self.model = model
        self.encoder_name = encoder_name

    def run(self, train, test):
        encoder = encoders[self.encoder_name]()                         
        train = encoder.fit_transform(train)
        test = encoder.transform(test)
    
        class_weight = utils.balanced_class_weights(train)
        model_instance = self.model(scoring='recall', class_weight=class_weight)

        model_instance.fit(train.X, train.y) 

        del encoder

        y_true = test.y
        y_pred = model_instance.predict(test.X)

        return y_true, y_pred


for encoder_name in encoders:
    for model in models:
        experiments[f'{model.name()}_{encoder_name}'] = Experiment(model, encoder_name)