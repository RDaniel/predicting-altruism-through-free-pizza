import data.encoders as enc
import model.clusteringclassification as cc
import model.machinelearning as ml
import numpy as np
import collections
import experiments.utils as utils

encoders = {
    'word2vec': lambda: enc.Word2VecEncoder('embeddings/vectors.bin'),
    'glove': lambda: enc.GloveEncoder('embeddings/glove.6B.300d.txt')
}

models = [
    # cc.ClusteringNN,
    # cc.create_cluster_classifier(ml.XGBoost),
    cc.create_cluster_classifier(ml.SupportVectorClassifier),
    # cc.create_cluster_classifier(ml.LogisticRegression)
]

model_kwargs = [
    # {
    #     'cluster_model_params': {},
    #     'bottom_fit_params': {
    #         'epochs_lstm': 10,
    #         'batch_size': 32
    #     },
    #     'classifier_params': {},
    #     'top_fit_params': {
    #         'epochs_dense': 100,
    #         'validation_split': 0.2,
    #         'class_weight': None
    #     }
    # },
    {
        'cluster_model_params': {},
        'bottom_fit_params': {
            'epochs_lstm': 10,
            'batch_size': 32
        },
        'classifier_params': {'scoring': 'recall', 'class_weight': None},
        'top_fit_params': {}
    },
    # {
    #     'cluster_model_params': {},
    #     'bottom_fit_params': {
    #         'epochs_lstm': 10,
    #         'batch_size': 32
    #     },
    #     'classifier_params': {'scoring': 'recall', 'class_weight': None},
    #     'top_fit_params': {}
    # },
    # {
    #     'cluster_model_params': {},
    #     'bottom_fit_params': {
    #         'epochs_lstm': 10,
    #         'batch_size': 32
    #     },
    #     'classifier_params': {'scoring': 'recall', 'class_weight': None},
    #     'top_fit_params': {}
    # }
]

experiments = {}

class Experiment:
    def __init__(self, model, kwargs, encoder_name):
        self.model = model
        self.kwargs = kwargs
        self.encoder_name = encoder_name

    def run(self, train, test):                        
        encoder = encoders[self.encoder_name]()

        features_to_ignore = ['timestamp', 'subreddits', 'posts_in_raop']
        numeric_encoder = enc.NumericAttributeEncoder(features_to_ignore)
        train_numeric = numeric_encoder.fit_transform(train)

        train = encoder.fit_transform(train)
        test = encoder.transform(test)
    
        class_weight = utils.balanced_class_weights(train)

        model_instance = self.model(enc.get_embedding(encoder))

        del encoder
        
        if 'class_weight' in self.kwargs['top_fit_params']:
            self.kwargs['top_fit_params']['class_weight'] = class_weight
        elif 'class_weight' in self.kwargs['classifier_params']:
            self.kwargs['classifier_params']['class_weight'] = class_weight
        
        model_instance.fit(train.X, train.y, cluster_dimensions=train_numeric.X, **self.kwargs)

        y_true = test.y
        y_pred = model_instance.predict(test.X)

        return y_true, y_pred

for encoder_name in encoders:
    for model, kwargs in zip(models, model_kwargs):
        experiments[f'{model.name()}_{encoder_name}'] = Experiment(model, kwargs, encoder_name)