import pickle
import tkinter
from tkinter import messagebox
from tkinter.scrolledtext import ScrolledText
from tkinter.ttk import Combobox

from data.data import Request, Corpus

TITLE = "Predicting altruism through free pizza"

ALGORITHM_FILE_DICT = {
    "Logistic regression": "models/log_reg",
    "XGBoost": "models/xgb",
    "CNN": "models/cnn",
}

ALGORITHM_EMBED_DICT = {
    "Logistic regression": "models/tfidf",
    "XGBoost": "models/numeric",
    "CNN": "models/glove",
}

window = tkinter.Tk()
window.title(TITLE)
window.geometry("800x600+500+300")
window.resizable(False, False)

lbl = tkinter.Label(window, text=TITLE.upper(), font=("Arial Bold", 18))
lbl.grid(column=0, row=0)

combo = Combobox(window)
combo['values'] = ("Logistic regression", "XGBoost", "CNN")
combo['state'] = 'readonly'
combo.grid(column=0, row=1, pady=20)
combo.current(0)

txt = ScrolledText(window, width=95, height=20)
txt.grid(column=0, row=2, padx=10)


def predict():
    selected_algorithm = combo.get()

    pizza_request = Request()
    pizza_request.text = txt.get('1.0', tkinter.END)
    corpus = Corpus([pizza_request], [])

    model_path = ALGORITHM_FILE_DICT.get(selected_algorithm)
    embed_path = ALGORITHM_EMBED_DICT.get(selected_algorithm)

    with open(embed_path, 'rb') as readfile:
        embedding = pickle.load(readfile)
        pizza_request = embedding.transform(corpus)

    with open(model_path, 'rb') as readfile:
        model = pickle.load(readfile)
        prediction = model.predict(pizza_request.X)[0]

    if prediction > 0.5:
        messagebox.showinfo('Prediction result', "Received free pizza!")
    else:
        messagebox.showinfo('Prediction result', "Didn't receive free pizza!")


def show_info():
    with open('project_info.txt', 'r') as file:
        data = file.read()
        messagebox.showinfo("About the project", data)


predict_button = tkinter.Button(window, text='Predict', command=predict)
predict_button.grid(column=0, row=3, pady=5)

about_button = tkinter.Button(window, text='About the project', command=show_info)
about_button.grid(column=0, row=5, pady=80)

window.mainloop()
