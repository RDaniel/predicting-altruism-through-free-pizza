import data.encoders as encoders
import model.deeplearning as dl
from data.data import create_request_corpus
from data.text_preprocessing import PreprocessingPipeline, LowerCaseStep
import pickle

DATA_FILE_NAME = 'data/pizza_request_dataset.json'

train, test = create_request_corpus(DATA_FILE_NAME)
pipeline = PreprocessingPipeline([LowerCaseStep()])
pipeline.run([train, test])

encoder = encoders.GloveEncoder('glove.6B.300d.txt')
train = encoder.fit_transform(train)
# test = encoder.transform(test)

# model = dl.SimpleCNN(encoders.get_embedding(encoder))
# model.fit(train.X, train.y, epochs=10, batch_size=64, validation_split=0.2, class_weight={1: 5, 0: 1})

filename = "gui/models/glove"
with open(filename, 'wb') as outfile:
    pickle.dump(encoder, outfile)