import os
import sys
import json
from scipy.stats import ttest_1samp
import csv

names = sys.argv[2:]
res_dict = {}

fields = [sys.argv[1]]

averages = {}

for name in names:
    for file in os.listdir(name):
        exp_name = file[:-5]
        with open(f'{name}/{file}') as myjson:
            jsondict_list = json.load(myjson)
            res_dict[exp_name] = {}
            for field in fields:
                res_dict[exp_name][field] = []
                for jsondict in jsondict_list[:-1]:
                    res_dict[exp_name][field].append(float(jsondict[field]))
                averages[exp_name] = jsondict_list[-1]

def matched_pair_t_test(array1, array2, alpha=0.1):
    diffs = [a1 - a2 for a1, a2 in zip(array1, array2)]
    return ttest_1samp(diffs, 0.0)[0] <= alpha

with open(f'results_{sys.argv[1]}.csv', 'w', newline='') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['exp1', 'exp2', 'metric', 'average1', 'average2', 't-test'])
    for exp_name1 in sorted(res_dict.keys()):
        for exp_name2 in sorted(res_dict.keys()):
            if exp_name1 != exp_name2:
                for field in fields:
                    writer.writerow([exp_name1, exp_name2, field, 
                        averages[exp_name1][f'avg_{field}'], averages[exp_name2][f'avg_{field}'],
                        matched_pair_t_test(res_dict[exp_name1][field], res_dict[exp_name2][field])])